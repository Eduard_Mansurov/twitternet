﻿using Classes;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Сводное описание для AbstractRepo
/// </summary>
public abstract class AbstractRepo
{
    public AbstractRepo()
    {
        //Session = NHibernateHelper.GetCurrentSession();
        entities = new Entities();
        
    }

    private Entities entities;

    public Entities Entities
    {
        get
        {
            return entities;
        }

        set
        {
            entities = value;
        }
    }

    /* private ISession session;

       

    protected ISession Session
    {
        get
        {
            return session;
        }

        set
        {
            session = value;
        }
    } */

    ~AbstractRepo()
    {
        //NHibernateHelper.CloseSession();
    }
/*
    public void save(Object entity)
    {
        ITransaction tx = session.BeginTransaction();
        session.Save(entity);
        tx.Commit();
    }
    */
}