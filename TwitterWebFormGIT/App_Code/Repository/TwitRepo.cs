﻿using Classes;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

/// <summary>
/// Сводное описание для TwitRepo
/// </summary>
public class TwitRepo : AbstractRepo
{
    public TwitRepo()
    {
        
    }
    
    public void add(twit newTwit)
    {
        Entities.twit.Add(newTwit);
        try { 
            Entities.SaveChanges();
        }
        catch (DbEntityValidationException ex)
        {
            foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
            {
                // Get entry

                DbEntityEntry entry = item.Entry;
                string entityTypeName = entry.Entity.GetType().Name;

                // Display or log error messages

                foreach (DbValidationError subItem in item.ValidationErrors)
                {
                    string message = string.Format("Error '{0}' occurred in {1} at {2}",
                             subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                    Console.WriteLine(message);
                }
            }
        }
    }
}