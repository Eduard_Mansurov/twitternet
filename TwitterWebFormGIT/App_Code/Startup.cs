﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TwitterWebFormGIT.Startup))]
namespace TwitterWebFormGIT
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
